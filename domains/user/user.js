const {Schema, model, SchemaTypes: {ObjectId}} = require('mongoose');

let userSchema = Schema(
    {
        name: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        phone: {
            type: String,
            required: true,
            unique: true
        },
        address: {
            type: String,
            required: false
        },
    },
    {timestamps: true}
);

exports.User = model('User', userSchema);
