const userDAL = require('./userDAL');
const errorHelper = require('../../../libraries/error');
const mongoQuery = require('../../../libraries/mongoQuery');

/**
 * Get List Data
 * @param {Object} query values for filtering needs
 */
const index = async (query) => {
    // get query params
    const {page, limit, search, sort_by, sort_type} = query;

    // init filters
    let filters = [{}];

    // filter search
    if (search && search !== '') {
        filters.push({
            $or: [
                {name: mongoQuery.searchLike(search)},
                {email: mongoQuery.searchLike(search)},
                {phone: mongoQuery.searchLike(search)},
            ],
        });
    }

    // sort
    const sort = mongoQuery.sortBy(sort_by, 'createdAt', sort_type, 'asc');

    // get data
    const users = await userDAL.list(filters, page, limit, sort);
    const totalFiltered = await userDAL.total(filters);

    // response
    return {
        data: users,
        meta: {
            total_filtered: totalFiltered.total,
        },
    };
};

/**
 * Create Data
 * @param {Object} body
 */
const create = async (body) => {
    let result = await userDAL.create(body);
    if (!result) errorHelper.throwInternalServerError("Create Failed");

    return result;
};

/**
 * Get Detail Data
 * @param {String} id
 */
const detail = async (id) => {
    const user = await userDAL.findById(id);
    if (!user) errorHelper.throwNotFound();
    return user;
};

/**
 * Update Data
 * @param {String} id
 * @param {Object} body
 */
const updateOne = async (id, body) => {
    const user = await userDAL.findById(id);
    if (!user) errorHelper.throwNotFound();

    let result = await userDAL.updateOne(id, body);
    if (!result) errorHelper.throwInternalServerError("Update Failed");

    return result;
};

/**
 * Delete Data
 * @param {String} id
 */
const deleteOne = async (id) => {
    const user = await userDAL.findById(id);
    if (!user) errorHelper.throwNotFound();

    let result = await userDAL.deleteOne(id);
    if (!result) errorHelper.throwInternalServerError("Delete Failed");

    return result;
};


module.exports = {
    index,
    create,
    detail,
    updateOne,
    deleteOne,
};
