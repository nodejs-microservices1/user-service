const Joi = require('joi');

const create = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    phone: Joi.string().required(),
    address: Joi.string().allow("", null),
});

const updateOne = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().email().required(),
    phone: Joi.string().required(),
    address: Joi.string().allow("", null),
});

module.exports = {
    create,
    updateOne,
};
