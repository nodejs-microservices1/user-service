const userService = require('./userService');
const errorHelper = require('../../../libraries/error');
const logger = require('../../../libraries/logger');
const respond = require('../../../libraries/respond');

/**
 * List Data
 * @param {Object} req express request object
 * @param {Object} res express response object
 */
const index = async (req, res) => {
    try {
        const result = await userService.index(req.query);
        return respond.responseSuccess(res, 'List Data', result.data, result.meta);
    } catch (e) {
        logger.info(e);
        return respond.responseError(res, e.statusCode, e.message);
    }
};

/**
 * Create New Data
 * @param {Object} req express request object
 * @param {Object} res express response object
 */
const create = async (req, res) => {
    try {
        const result = await userService.create(req.body);
        return respond.responseCreated(res, 'Data Created', result, undefined);
    } catch (e) {
        if (e.name === errorHelper.UNPROCESSABLE_ENTITY) {
            return respond.responseUnprocessableEntity(res, e.message);
        }
        logger.info(e);
        return respond.responseError(res, e.statusCode, e.message);
    }
};

/**
 * Detail Data
 * @param {Object} req express request object
 * @param {Object} res express response object
 */
const detail = async (req, res) => {
    try {
        const result = await userService.detail(req.params.id);
        return respond.responseSuccess(res, 'Detail Data', result, undefined);
    } catch (e) {
        if (e.name === errorHelper.NOT_FOUND) {
            return respond.responseNotFound(res, e.message);
        }
        logger.info(e);
        return respond.responseError(res, e.statusCode, e.message);
    }
};

/**
 * Update One Data
 * @param {Object} req express request object
 * @param {Object} res express response object
 */
const updateOne = async (req, res) => {
    try {
        const result = await userService.updateOne(req.params.id, req.body);
        return respond.responseSuccess(res, 'Data Updated', result, undefined);
    } catch (e) {
        if (e.name === errorHelper.NOT_FOUND) {
            return respond.responseNotFound(res, e.message);
        }
        if (e.name === errorHelper.UNPROCESSABLE_ENTITY) {
            return respond.responseUnprocessableEntity(res, e.message);
        }
        logger.info(e);
        return respond.responseError(res, e.statusCode, e.message);
    }
};

/**
 * Delete One Data
 * @param {Object} req express request object
 * @param {Object} res express response object
 */
const deleteOne = async (req, res) => {
    try {
        await userService.deleteOne(req.params.id);
        return respond.responseSuccess(res, 'Data Deleted', undefined, undefined);
    } catch (e) {
        if (e.name === errorHelper.NOT_FOUND) {
            return respond.responseNotFound(res, e.message);
        }
        if (e.name === errorHelper.UNPROCESSABLE_ENTITY) {
            return respond.responseUnprocessableEntity(res, e.message);
        }
        logger.info(e);
        return respond.responseError(res, e.statusCode, e.message);
    }
};

module.exports = {
    index,
    create,
    detail,
    updateOne,
    deleteOne,
};
