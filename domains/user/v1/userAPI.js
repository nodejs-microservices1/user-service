const express = require('express');
const userController = require('./userController');
const userValidation = require('./userValidation');
const validation = require('../../../middlewares/validation');

const router = express.Router();

router.get(
    '/',
    userController.index
);

router.post(
    '/',
    validation(userValidation.create),
    userController.create
);

router.get(
    '/:id',
    userController.detail
);

router.put(
    '/:id',
    validation(userValidation.updateOne),
    userController.updateOne
);

router.delete(
    '/:id',
    userController.deleteOne
);

module.exports = router;
